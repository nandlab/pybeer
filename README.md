# Riddle Safe and Beer
## Dependencies
```
sudo apt install jq mosquitto-clients python3-gst-1.0
pip3 install paho-mqtt playsound
```
## GPIO simulation on PC
```
pip3 install git+https://github.com/nosix/raspberry-gpio-emulator/
```
## Usage
```
python3 beer.py
```
## Enable Autostart
```
mkdir -p ~/.config/autostart && ln -s -t ~/.config/autostart ~/git/pybeer/pybeer.desktop
```
